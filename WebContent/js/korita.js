function loadingBackdrop(){
	$('<div class="modal-backdrop backdrop-loading"></div>').appendTo(document.body);
}
function notokBackdrop(){
	$('<div class="modal-backdrop backdrop-fail"></div>').appendTo(document.body);
}
function okBackdrop(){
	$('<div class="modal-backdrop backdrop-success"></div>').appendTo(document.body);
}
function removeBackdrop(){
	$(".modal-backdrop").remove();
}
function waitRemoveBackdrop(){
	setTimeout(removeBackdrop, 1000);
}