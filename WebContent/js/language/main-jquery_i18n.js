jQuery(document).ready(function() {
  var update_texts = function() {
    $('body').i18n();
	$('#error').text($.i18n('errorMe','E2'));
  };
  
  $.i18n().locale = navigator.language;
  
  $('.lang-switch').click(function(e) {
    e.preventDefault();
    $.i18n().locale = $(this).data('locale');
    update_texts();
  });

  $.i18n().load({
    'en': {
      'errorMe': '{{serverresponse:$1|Error1 En|Error2 En}}',
      'sharethewayyouwant': 'Share the way you want!',
      'signup' :'Sign up',
      'login' :'Log in',
      'share' :'Share',
      'Share what you want. Find opportunities around you.' :'Share what you want. Find opportunities around you.',
      'Sign up to Share' :'Sign up to Share',
      'Connect' :'Connect',
      'Connect and Save Money.' :'Connect and Save Money.',
      'Sign up to Connect' :'Sign up to Connect',
      'About' :'About',
      'Help' :'Help',
      'Contact' :'Contact',
      'Terms & Conditions' :'Terms & Conditions',
      'Already a Member ?':'Already a Member ?',
      'signupok': 'Congratulations , Signup is completed, please proceed with',
      'signupnotok' : 'Signup Failed, Please try Again, or else ',
      'notamember': 'Not yet a member? Sign up for free!',
      'firstname': 'First Name',
      'lastname': 'Last Name',
      'email': 'Email',
      'password': 'Password',
      'confirmpassword': 'Confirm password',
      'help': 'Help',
      'loginquestion': "What's your email and password?",
      'forgotpassword': "Forgot password?",
      'forgotquestion': "What's your registered email ?",
      'resetpassword': "Send me reset password link",
      'magiclink': "I want a magic link",
      'shareaservice': "Share a Service",
      'connectaservice': "Connect a Service",
      'hello': "Hello",
      'dashboard': "Dashboard",
      'sharedservices': "Shared Services",
      'connectedservices': "Connected Services",
      'notification': "Notification",
      'money': "Money",
      'profile': "Profile",
      'logout': "Log out",
      'myshared': "History of my sharing",
      'connectionrequested': "Connection Requested",
      'connectingservice': "Connecting Service",
      'sharingservice': "Sharing Service",
      'signuperrormessage': '{{serverresponse:$1|There is a Technical Error|This Email is already Registered with us}}',
      'viewprofile': "View my profile",
    }, 
    'de': {
      'errorMe': '{{serverresponse:$1|DE1|DE2}}',
      'sharethewayyouwant': 'Teilen Sie, wie Sie m�chten!',
      'signup' :'Anmelden',
      'login' :'Einloggen',
      'share' :'Aktie',
      'Share what you want. Find opportunities around you.' :'Teilen Sie, was Sie m�chten. Finden Sie M�glichkeiten um Sie herum.',
      'Sign up to Share' :'Melden Sie sich zu teilen',
      'Connect' :'Anschlie�en',
      'Connect and Save Money.' :'Schlie�en Sie und Geld sparen.',
      'Sign up to Connect' :'Melden Sie sich zu verbinden',
      'About' :'Ungef�hr um',
      'Help' :'Hilfe',
      'Contact' :'Kontakt',
      'Terms & Conditions' :'Gesch�ftsbedingungen',
      'Already a Member ?':'Schon ein Mitglied ?',
      'signupok': 'Herzlichen Gl�ckwunsch, Anmelden abgeschlossen ist, gehen Sie bitte mit',
      'signupnotok' : 'Registrieren fehlgeschlagen, versuchen Sie es erneut, oder ',
      'notamember': 'Noch kein Mitglied? Melden Sie sich kostenlos an!',
      'firstname': 'Vorname',
      'lastname': 'Nachname',
      'email': 'E-Mail-Addresse',
      'password': 'Passwort',
      'confirmpassword': 'Passwort best�tigen',
      'help': 'Hilfe',
      'loginquestion': "Wie lautet Ihre E-Mail-Adresse und Ihr Passwort?",
      'forgotpassword': "Passwort vergessen?",
      'forgotquestion': "Wie lautet Ihre registrierte E-Mail-Adresse?",
      'resetpassword': "Senden Sie mir den Link zum Zur�cksetzen des Passworts",
      'magiclink': "Ich m�chte eine magische Verbindung",
      'shareaservice': "Teilen Sie einen Service",
      'connectaservice': "Einen Dienst anschlie�en",
      'hello': "Hallo",
      'dashboard': "Instrumententafel",
      'sharedservices': "Geteilte Dienstleistungen",
      'connectedservices': "Verbundene Dienste",
      'notification': "Benachrichtigung",
      'money': "Geld",
      'profile': "Profil",
      'logout': "Ausloggen",
      'myshared': "Geschichte meines Teilens",
      'connectionrequested': "Verbindung angefordert",
      'connectingservice': "Verbindungsdienst",
      'sharingservice': "Freigabedienst",
      'signuperrormessage': '{{serverresponse:$1|Es liegt ein technischer Fehler vor|Diese Email ist bereits bei uns registriert}}',
      'viewprofile': "Mein Profil ansehen",
    }
  });

  update_texts();
});