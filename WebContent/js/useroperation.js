function signUpFormValidation() {
	var missingFields = false;
	document.getElementById("errorFname").style.display = "none";
	document.getElementById("errorEmail").style.display = "none";
	document.getElementById("errorPassword").style.display = "none";

	if (!(document.getElementById("registration_firstname").checkValidity())) {
		missingFields = true;
		document.getElementById("errorFname").innerHTML = document
				.getElementById("registration_firstname").validationMessage;
		document.getElementById("errorFname").style.display = "block";
	}
	if (!(document.getElementById("registration_email").checkValidity())) {
		missingFields = true;
		document.getElementById("errorEmail").innerHTML = document
				.getElementById("registration_email").validationMessage;
		document.getElementById("errorEmail").style.display = "block";
	}
	if (!(document.getElementById("registration_password_first")
			.checkValidity())) {
		missingFields = true;
		document.getElementById("errorPassword").innerHTML = document
				.getElementById("registration_password_first").validationMessage;
		document.getElementById("errorPassword").style.display = "block";
	}
	if (missingFields) {
		return false;
	}
	registerUser();
}

function registerUser() {
	loadingBackdrop();
	document.getElementById("sb").style.display = "none";
	$.ajax({
				url : '/cinkandshare/signup', 
				type : "POST", 
				contentType : 'application/json; charset=utf-8',
				dataType : 'json', // data type
				data : JSON.stringify({
					"firstName" : $('#registration_firstname').val(),
					"email" : $('#registration_email').val(),
					"password" : $('#registration_password_first').val(),
				}),
				success : function(data) {
					if (data.Result != 'OK') {
						document.getElementById("sb").style.display = "block";
						removeBackdrop();
						notokBackdrop();
						document.getElementById("signupResponse").innerHTML=data.Message;
						waitRemoveBackdrop();
                    }else{
						removeBackdrop();
						okBackdrop();
						document.getElementById("signupResponse").innerHTML="Sucessfully signedup";
						waitRemoveBackdrop();
                    }
				},
				error : function(
						xhr, resp,
						text) {
					document.getElementById("sb").style.display = "block";
					removeBackdrop();
					notokBackdrop();
					waitRemoveBackdrop();
				}
			});
}
function loginFormValidation() {
	var missingFields = false;
	document.getElementById("errorEmail").style.display = "none";
	document.getElementById("errorPassword").style.display = "none";

	if (!(document.getElementById("login_email").checkValidity())) {
		missingFields = true;
		document.getElementById("errorEmail").innerHTML = document
				.getElementById("login_email").validationMessage;
		document.getElementById("errorEmail").style.display = "block";
	}
	if (!(document.getElementById("login_password")
			.checkValidity())) {
		missingFields = true;
		document.getElementById("errorPassword").innerHTML = document
				.getElementById("login_password").validationMessage;
		document.getElementById("errorPassword").style.display = "block";
	}
	if (missingFields) {
		return false;
	}
	loginUser();
}
function loginUser() {
	loadingBackdrop();
	document.getElementById("lb").style.display = "none";
	$.ajax({
				url : '/cinkandshare/login', 
				type : "POST", 
				contentType : 'application/json; charset=utf-8',
				dataType : 'json', // data type
				data : JSON.stringify({
					"email" : $('#login_email').val(),
					"password" : $('#login_password').val(),
				}),
				success : function(data) {
					if (data.Result != 'OK') {
						document.getElementById("lb").style.display = "block";
						removeBackdrop();
						notokBackdrop();
						document.getElementById("loginResponse").innerHTML=data.Message;
						waitRemoveBackdrop();
                    }else{
						removeBackdrop();
						okBackdrop();
						document.getElementById("loginResponse").innerHTML="Login Success";
						waitRemoveBackdrop();
                    }
				},
				error : function(
						xhr, resp,
						text) {
					document.getElementById("lb").style.display = "block";
					removeBackdrop();
					notokBackdrop();
					waitRemoveBackdrop();
				}
			});
}