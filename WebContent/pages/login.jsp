<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Login</title>
<style type="text/css">
#errorEmail, #errorPassword {
	display: none;
}
</style>
<link href="/css/korita.css" rel="stylesheet" type="text/css">
<link href="/css/design.css" rel="stylesheet" type="text/css">

<script src="/js/korita.js" type="text/javascript"></script>
<script src="/js/useroperation.js" type="text/javascript"></script>
<script src="/js/language/jquery.js" type="text/javascript"></script>
<script src="/js/language/CLDRPluralRuleParser.js"></script>
<script src="/js/language/jquery.i18n.js"></script>
<script src="/js/language/jquery.i18n.messagestore.js"></script>
<script src="/js/language/jquery.i18n.fallbacks.js"></script>
<script src="/js/language/jquery.i18n.language.js"></script>
<script src="/js/language/jquery.i18n.parser.js"></script>
<script src="/js/language/jquery.i18n.emitter.js"></script>
<script src="/js/language/jquery.i18n.emitter.bidi.js"></script>
<script src="/js/language/main-jquery_i18n.js"></script>
</head>
<body>
	<div class="page">
	<a href="/Signup">Signup</a> | <a href="/Login">Login</a>
	<h1 id="loginResponse" class="size24 line26 u-alignCenter margin-bottom"></h1>
		<div id ="loginform" class="form-register">
			<form class="form-register-container padding-half-top">
				<h1 class="size24 line26 u-alignCenter margin-bottom"
					data-i18n="loginquestion"></h1>

				<div class="jsx-1923214227 kirk-textField ">
					<div class="jsx-1923214227 kirk-textField-wrapper">
						<input type="email" data-i18n="[placeholder]email" name="email"
							id="login_email"
							pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$"
							class="jsx-1923214227 " required>
					</div>
					<p id="errorEmail" class="alert alert-error no-icon  "></p>
				</div>
				<div class="jsx-1923214227 kirk-textField ">
					<div class="jsx-1923214227 kirk-textField-wrapper">
						<input type="password" data-i18n="[placeholder]password"
							name="password" id="login_password"
							class="jsx-1923214227" required>
					</div>
					<p id="errorPassword" class="alert alert-error no-icon "></p>
				</div>
				<div class="button-wrapper m-xl text-center" id="lb">
					<button class="kirk-button kirk-button-primary" type="button"
						id="login" title="" onclick="loginFormValidation()">
						<span class="jsx-4075288211" data-i18n="login">Login</span>
					</button>
				</div>

				Language <a href="#" class="lang-switch"
					data-locale="en">English</a> | <a href="#" class="lang-switch"
					data-locale="de">German</a>
			</form>
		</div>
	</div>
</body>
</html>
